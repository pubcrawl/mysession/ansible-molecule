# ansible-molecule

## Setup 

```bash
#Setup Container (ubuntu:focal)
apt-get update
apt-get -y dist-upgrade
apt-get install -y rsync python3-ldap python3-pip python3-aiohttp docker.io vim

#Setup Molecule und Ansible
pip3 install yamllint ansible-lint==5.4.0 ansible==2.9.27 molecule molecule[docker,lint] molecule-docker

#Init Molecule:
molecule init role molecule.test --driver-name docker

molecule create #Testumgebung erzeugen
molecule list #Anzeigen der Testinstanzen
molecule login -h CONTAINERNAME # alternativ über: docker exec -e COLUMNS=246 -e LINES=27 -e TERM=bash -e TERM=xterm -ti CONTAINERNAME bash
molecule destroy #Abbau der Testumgebung
```

## Links

https://github.com/ansible-community/molecule <br>
https://molecule.readthedocs.io/en/latest/
